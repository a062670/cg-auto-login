﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading;

namespace CG_auto_login
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Thread th;
        CDmSoft dm;

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (th != null)
            {
                th.Abort();
            }
        }

        private void initForm(object sender, EventArgs e)
        {
            dm = new CDmSoft();

            dm.SetDict(0, "assets/font.txt");
            dm.SetDict(1, "assets/font1.txt");

            cb_loginCol.SelectedIndex = 0;
            cb_loginRow.SelectedIndex = 1;

            cb_move10.SelectedIndex = 0;
        }

        private void disableUI()
        {
            panel_actions.Enabled = false;
        }

        private void enableUI()
        {
            panel_actions.Enabled = true;
        }

        //////////
        /// 共用類
        //////////

        private void delay(int ms)
        {
            Random rand = new Random();
            int num = rand.Next(0, 100);
            Thread.Sleep(ms + num);
        }

        private void closeAll()
        {
            dm.KeyDownChar("SHIFT");
            Thread.Sleep(200);
            dm.KeyPressChar("F12");
            Thread.Sleep(200);
            dm.KeyPressChar("F12");
            Thread.Sleep(200);
            dm.KeyUpChar("SHIFT");
            Thread.Sleep(200);
        }
        // 說話
        private void speak(string text)
        {
            dm.SendString(hwnd, text);
            dm.KeyPressChar("ENTER");
        }

        public class AutoClosingMessageBox
        {
            System.Threading.Timer _timeoutTimer;
            string _caption;
            AutoClosingMessageBox(string text, string caption, int timeout)
            {
                _caption = caption;
                _timeoutTimer = new System.Threading.Timer(OnTimerElapsed,
                    null, timeout, System.Threading.Timeout.Infinite);
                MessageBox.Show(text, caption);
            }

            public static void Show(string text, string caption, int timeout)
            {
                new AutoClosingMessageBox(text, caption, timeout);
            }

            void OnTimerElapsed(object state)
            {
                IntPtr mbWnd = FindWindow(null, _caption);
                if (mbWnd != IntPtr.Zero)
                    SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
                _timeoutTimer.Dispose();
            }
            const int WM_CLOSE = 0x0010;
            [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
            static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
            [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
            static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
        }

        private void goHome()
        {
            // 點系統
            dm.MoveTo(510, 465);
            Thread.Sleep(1000);
            dm.LeftClick();
            Thread.Sleep(1000);

            // 找登出
            string logoutPos = dm.FindPicE(0, 0, 640, 480, "assets/logout.bmp", "000000", 0.9, 0);
            string[] logoutPosSplit = logoutPos.Split('|');
            if (logoutPosSplit[0] == "-1")
            {
                dm.Play("assets/wav/找不到登出按鈕.wav");
                return;
            }

            int logoutX = Int32.Parse(logoutPosSplit[1]);
            int logoutY = Int32.Parse(logoutPosSplit[2]);

            // 點登出
            dm.MoveTo(logoutX + 10, logoutY + 5);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(1000);

            // 找回登入點
            string goHomePos = dm.FindPicE(0, 0, 640, 480, "assets/goHome.bmp", "000000", 0.9, 0);
            string[] goHomePosSplit = goHomePos.Split('|');
            if (goHomePosSplit[0] == "-1")
            {
                dm.Play("assets/wav/找不到回登入點按鈕.wav");
                return;
            }

            int goHomeX = Int32.Parse(goHomePosSplit[1]);
            int goHomeY = Int32.Parse(goHomePosSplit[2]);

            // 點回登入點
            dm.MoveTo(goHomeX + 10, goHomeY + 5);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(1000);
        }

        private void stopTh(object sender, EventArgs e)
        {
            if (th != null)
            {
                th.Abort();
            }
        }

        public class MakeUIPos
        {
            public bool is_OK = false;

            public int btn_runX = 0;
            public int btn_runY = 0;

            public int item0X = 0;
            public int item0Y = 0;
        }

        private MakeUIPos getMakeUIPos()
        {
            MakeUIPos makeUIPos = new MakeUIPos();
            string pos = dm.FindPicE(0, 0, 640, 480, "assets/makeUI.bmp", "000000", 0.9, 0);
            string[] posSplit = pos.Split('|');
            if (posSplit[0] == "-1")
            {
                dm.Play("assets/wav/找不到製作視窗.wav");
                return makeUIPos;
            }

            int x = Int32.Parse(posSplit[1]);
            int y = Int32.Parse(posSplit[2]);

            makeUIPos.is_OK = true;

            makeUIPos.btn_runX = x - 225;
            makeUIPos.btn_runY = y + 205;

            makeUIPos.item0X = x + 55;
            makeUIPos.item0Y = y + 40;

            return makeUIPos;
        }

        //////////
        /// 綁視窗
        //////////

        int hwnd = 0;
        private void bindWindow(object sender, EventArgs e)
        {
            disableUI();

            lb_bindWindowStatus.Text = "";
            Thread.Sleep(3000);

            hwnd = dm.GetMousePointWindow();
            if (dm.GetWindowClass(hwnd) != "魔力寶貝")
            {
                lb_bindWindowStatus.Text = "找不到視窗";
                return;
            }

            int dm_ret = dm.BindWindowEx(hwnd, "gdi", "windows", "windows", "", 0);
            // int dm_ret = dm.BindWindow(hwnd, "normal", "normal", "dx", 0);
            if (dm_ret != 1)
            {
                lb_bindWindowStatus.Text = "綁定失敗";
                return;
            }
            lb_bindWindowStatus.Text = "綁定成功";

            enableUI();
        }

        ///////////
        /// 截圖
        ///////////

        private void captureRun(object sender, EventArgs e)
        {
            int x1 = Int32.Parse(tb_captureX1.Text);
            int x2 = Int32.Parse(tb_captureX2.Text);
            int y1 = Int32.Parse(tb_captureY1.Text);
            int y2 = Int32.Parse(tb_captureY2.Text);
            string file = "capture.bmp";
            dm.Capture(x1, y1, x2, y2, file);
        }

        //////////
        /// 登登樂
        //////////

        private void loginRun(object sender, EventArgs e)
        {
            int col = Int32.Parse((cb_loginCol.SelectedItem ?? 1).ToString());
            int row = Int32.Parse((cb_loginRow.SelectedItem ?? 2).ToString());

            int x = 50 + col * 100;
            int y = 83 + row * 32;

            th = new Thread(() =>
            {
                login(x, y);
            });
            th.Start();
        }

        private void login(int x, int y)
        {
            dm.MoveToEx(x - 5, y - 5, 10, 10);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(5000);
            if (!checkLigin())
            {
                Thread.Sleep(2000);
                login(x, y);
            }
        }

        private Boolean checkLigin()
        {
            if (dm.GetColor(400, 400) == "000000")
            {
                // AutoClosingMessageBox.Show("登入失敗，等待重試", "CG", 1000);
                dm.MoveToEx(320, 272, 10, 6);
                Thread.Sleep(200);
                dm.LeftClick();
                return false;
            }
            AutoClosingMessageBox.Show("登入成功", "CG", 1000);
            return true;
        }

        //////////
        /// 買麻布
        //////////

        private void linenRun(object sender, EventArgs e)
        {
            int times = Int32.Parse(tb_linen.Text);
            // 點 NPC
            dm.MoveTo(250, 185);
            Thread.Sleep(100);
            dm.RightClick();
            Thread.Sleep(1000);

            th = new Thread(() =>
            {
                linen(times);
            });
            th.Start();
        }

        private void linen(int times)
        {
            if (times <= 0)
            {
                dm.Play("assets/wav/麻布已完成購買.wav");
                return;
            }

            // 點買
            dm.MoveToEx(325, 260, 10, 5);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(1000);

            // 找商店視窗位置
            string storePos = dm.FindPicE(0, 0, 640, 480, "assets/storeUI.bmp", "000000", 0.9, 0);
            string[] storePosSplit = storePos.Split('|');
            if (storePosSplit[0] == "-1")
            {
                dm.Play("assets/wav/麻布找不到商店視窗.wav");
                return;
            }

            int storeX = Int32.Parse(storePosSplit[1]);
            int storeY = Int32.Parse(storePosSplit[2]);

            // 點 +
            dm.MoveToEx(storeX - 170, storeY + 80, 4, 4);
            Thread.Sleep(100);
            dm.LeftDown();
            Thread.Sleep(3000);

            // 放開 +
            dm.LeftUp();
            Thread.Sleep(100);

            // 點確定
            dm.MoveToEx(storeX + 64, storeY + 326, 4, 4);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(1000);

            // 點是
            dm.MoveToEx(130, 383, 10, 4);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(1000);

            linen(times - 1);
        }

        ///////////
        /// 1A鞋
        ///////////

        private void shoe1aRun(object sender, EventArgs e)
        {
            string pos = dm.FindPicE(0, 0, 640, 480, "assets/makeUI.bmp", "000000", 0.9, 0);
            string[] posSplit = pos.Split('|');
            if (posSplit[0] == "-1")
            {
                dm.Play("assets/wav/找不到製作視窗.wav");
                return;
            }

            int x = Int32.Parse(posSplit[1]);
            int y = Int32.Parse(posSplit[2]);

            int btn_runX = x - 225;
            int btn_runY = y + 205;

            int item0X = x + 55;
            int item0Y = y + 40;

            th = new Thread(() =>
            {
                shoe1a(btn_runX, btn_runY, item0X, item0Y, 0);
            });
            th.Start();
        }

        private void shoe1a(int btn_runX, int btn_runY, int item0X, int item0Y, int index)
        {
            if (index >= 18)
            {
                AutoClosingMessageBox.Show("製作完畢", "CG", 1000);
                return;
            }

            // 鹿皮位置
            int itemAX = item0X + (index % 10) / 2 * 50;
            int itemAY = item0Y + index / 10 * 50;

            // 麻布位置
            int itemBX = item0X + index / 10 * 50;
            int itemBY = item0Y + 2 * 50;

            // 點鹿皮
            dm.MoveTo(itemAX, itemAY);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(200);

            // 點麻布
            dm.MoveTo(itemBX, itemBY);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(200);

            // 點製作
            dm.MoveTo(btn_runX, btn_runY);
            Thread.Sleep(100);
            dm.LeftClick();

            // 等15秒
            Thread.Sleep(15000);

            // 點重來
            dm.MoveTo(btn_runX, btn_runY);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(500);

            // 下一輪
            shoe1a(btn_runX, btn_runY, item0X, item0Y, index + 1);
        }

        ///////////
        /// 鑑定魚
        ///////////
        private void identifyFishRun(object sender, EventArgs e)
        {
            int times = Int32.Parse(tb_identifyFish.Text);

            th = new Thread(() =>
            {
                identifyFish(times);
            });
            th.Start();
        }

        private void identifyFish(int times)
        {
            if (times <= 0)
            {
                goHome();
                dm.Play("assets/wav/鑑定完成了哦.wav");
                return;
            }

            Boolean isFind = false;

            // 點 NPC
            dm.MoveToEx(318, 20, 10, 4);
            Thread.Sleep(100);
            dm.RightClick();
            Thread.Sleep(1000);

            // 檢查 NPC 對話視窗
            isFind = false;
            for (int i = 0; i < 5; i++)
            {
                if (dm.GetColor(270, 320) == "346875")
                {
                    isFind = true;
                    break;
                }
                Thread.Sleep(1000);
            }
            if (!isFind)
            {
                dm.Play("assets/wav/鑑定魚店NPC不理我.wav");
                return;
            }

            // 點是
            dm.MoveToEx(245, 315, 10, 2);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(2000);

            // 找鑑定
            string skillPos = dm.FindPicE(0, 0, 640, 480, "assets/skillIdentify.bmp", "000000", 0.9, 0);
            string[] skillPosSplit = skillPos.Split('|');
            if (skillPosSplit[0] == "-1")
            {
                dm.Play("assets/wav/鑑定找不到技能.wav");
                return;
            }
            int skillX = Int32.Parse(skillPosSplit[1]) + 13;
            int skillY = Int32.Parse(skillPosSplit[2]) + 7;

            // 點鑑定
            dm.MoveToEx(skillX, skillY, 10, 5);
            Thread.Sleep(200);
            dm.LeftClick();
            Thread.Sleep(1500);

            // 找鑑定框
            string skillUIPos = dm.FindPicE(0, 0, 640, 480, "assets/skillUI.bmp", "000000", 0.9, 0);
            string[] skillUIPosSplit = skillUIPos.Split('|');
            if (skillUIPosSplit[0] == "-1")
            {
                dm.Play("assets/wav/鑑定找不到框.wav");
                return;
            }
            int skillUIX = Int32.Parse(skillUIPosSplit[1]) + 13;
            int skillUIY = Int32.Parse(skillUIPosSplit[2]) + 7;

            // 點魚
            dm.MoveToEx(skillUIX + 95, skillUIY + 35, 10, 5);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(500);

            // 點執行
            dm.MoveToEx(skillUIX - 195, skillUIY + 200, 10, 2);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(100);

            // 滑鼠移開
            dm.MoveToEx(skillUIX, skillUIY, 10, 2);
            Thread.Sleep(9000);

            // 檢查 鑑定重試的按鈕
            isFind = false;
            for (int i = 0; i < 10; i++)
            {
                if (dm.GetColor(skillUIX - 195, skillUIY + 200) == "333f75")
                {
                    isFind = true;
                    break;
                }
                Thread.Sleep(1000);
            }
            if (!isFind)
            {
                dm.Play("assets/wav/鑑定找不到蟲試的按鈕.wav");
                return;
            }

            // 點重試
            dm.MoveToEx(skillUIX - 195, skillUIY + 200, 10, 2);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(500);

            // 點魚
            dm.MoveToEx(skillUIX + 95, skillUIY + 35, 10, 5);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(500);

            // 點地板
            dm.MoveToEx(318, 20, 10, 4);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(1000);

            identifyFish(times - 1);
        }

        ///////////
        /// 2B料理(法包)
        ///////////
        private void cooking2bRun(object sender, EventArgs e)
        {
            string pos = dm.FindPicE(0, 0, 640, 480, "assets/makeUI.bmp", "000000", 0.9, 0);
            string[] posSplit = pos.Split('|');
            if (posSplit[0] == "-1")
            {
                AutoClosingMessageBox.Show("找不到製作視窗", "CG", 1000);
                return;
            }

            int x = Int32.Parse(posSplit[1]);
            int y = Int32.Parse(posSplit[2]);

            int btn_runX = x - 225;
            int btn_runY = y + 205;

            int item0X = x + 55;
            int item0Y = y + 40;

            th = new Thread(() =>
            {
                cooking2b(btn_runX, btn_runY, item0X, item0Y, 0);
            });
            th.Start();
        }

        private void cooking2b(int btn_runX, int btn_runY, int item0X, int item0Y, int index)
        {
            if (index >= 10)
            {
                AutoClosingMessageBox.Show("製作完畢", "CG", 1000);
                return;
            }

            // 小麥粉位置
            int itemAX = item0X + (index % 10) / 2 * 50;
            int itemAY = item0Y + index / 10 * 50;

            // 牛奶位置
            int itemBX = item0X + (index % 10) / 2 * 50;
            int itemBY = item0Y + 50;

            // 鹽巴位置
            int itemCX = item0X + (index % 10) / 2 * 50;
            int itemCY = item0Y + 100;

            // 點小麥粉
            dm.MoveTo(itemAX, itemAY);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(300);

            // 點牛奶
            dm.MoveTo(itemBX, itemBY);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(300);

            // 點鹽巴
            dm.MoveTo(itemCX, itemCY);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(300);

            // 點製作
            dm.MoveTo(btn_runX, btn_runY);
            Thread.Sleep(100);
            dm.LeftClick();

            // 等15秒
            Thread.Sleep(16500);

            // 點重來
            dm.MoveTo(btn_runX, btn_runY);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(1000);

            // 下一輪
            cooking2b(btn_runX, btn_runY, item0X, item0Y, index + 1);
        }

        ///////////
        /// 移動遇怪
        ///////////
        private void moveRun(object sender, EventArgs e)
        {
            int posE = Int32.Parse(tb_moveE.Text);
            int posS = Int32.Parse(tb_moveS.Text);

            List<int> dirList = new List<int>();

            if (cb_move1.Checked)
            {
                dirList.Add(1);
            }
            if (cb_move2.Checked)
            {
                dirList.Add(2);
            }
            if (cb_move3.Checked)
            {
                dirList.Add(3);
            }
            if (cb_move4.Checked)
            {
                dirList.Add(4);
            }
            if (cb_move6.Checked)
            {
                dirList.Add(6);
            }
            if (cb_move7.Checked)
            {
                dirList.Add(7);
            }
            if (cb_move8.Checked)
            {
                dirList.Add(8);
            }
            if (cb_move9.Checked)
            {
                dirList.Add(9);
            }

            th = new Thread(() =>
            {
                moveToPos(posE, posS);
                move(dirList);
            });
            th.Start();
        }

        private void moveToPos(int posE, int posS)
        {
            if (posE == 0 || posS == 0)
            {
                return;
            }

            // 切換字形庫
            dm.UseDict(1);

            // 找地圖框
            string mapUIPos = dm.FindPicE(0, 0, 640, 480, "assets/mapUI.bmp", "000000", 0.9, 0);
            string[] mapUIPosSplit = mapUIPos.Split('|');
            if (mapUIPosSplit[0] == "-1")
            {
                // 找不到，自己開
                // 關閉所有視窗
                closeAll();

                // 打開地圖
                dm.KeyDownChar("CTRL");
                Thread.Sleep(200);
                dm.KeyPressChar("Q");
                Thread.Sleep(200);
                dm.KeyUpChar("CTRL");
                Thread.Sleep(200);
                dm.KeyPressChar("back");

                mapUIPos = dm.FindPicE(0, 0, 640, 480, "assets/mapUI.bmp", "000000", 0.9, 0);
                mapUIPosSplit = mapUIPos.Split('|');
            }


            // 找到地圖框
            if (mapUIPosSplit[0] == "-1")
            {
                dm.Play("assets/wav/找不到地圖.wav");
                return;
            }
            int mapUIX = Int32.Parse(mapUIPosSplit[1]) + 13;
            int mapUIY = Int32.Parse(mapUIPosSplit[2]) + 7;

            // 擷取東
            string mapStrE = dm.Ocr(mapUIX - 38, mapUIY + 11, mapUIX - 17, mapUIY + 22, "FFFFFF", 1.0);
            mapStrE = mapStrE.Replace('-', '0');
            int mapE = Int32.Parse(mapStrE);

            // 擷取南
            string mapStrS = dm.Ocr(mapUIX - 38 + 108, mapUIY + 11, mapUIX - 17 + 108, mapUIY + 22, "FFFFFF", 1.0);
            mapStrS = mapStrS.Replace('-', '0');
            int mapS = Int32.Parse(mapStrS);

            if (mapE < posE && mapS < posS)
            {
                // 往東南
                moveClick(377, 230);
            }
            else if (mapE < posE && mapS > posS)
            {
                // 往東北
                moveClick(310, 185);
            }
            else if (mapE > posE && mapS < posS)
            {
                // 往西南
                moveClick(310, 284);
            }
            else if (mapE > posE && mapS > posS)
            {
                // 往西北
                moveClick(245, 230);
            }
            else if (mapE < posE)
            {
                // 往東
                moveClick(342, 210);
            }
            else if (mapE > posE)
            {
                // 往西
                moveClick(278, 252);
            }
            else if (mapS < posS)
            {
                // 往南
                moveClick(350, 260);
            }
            else if (mapS > posS)
            {
                // 往北
                moveClick(285, 213);
            }
        }

        private void move(List<int> dirList)
        {
            string color = dm.GetColor(600, 10);
            if (color == "faf7ed" || color == "000000")
            {
                return;
            }

            Random rand = new Random();
            int num = rand.Next(0, dirList.Count() - 1);

            switch (dirList[num])
            {
                case 1:
                    // 北
                    moveClick(285, 213);
                    moveClick(350, 260);
                    break;
                case 2:
                    // 東北
                    moveClick(310, 185);
                    moveClick(310, 284);
                    break;
                case 3:
                    // 東
                    moveClick(342, 210);
                    moveClick(278, 252);
                    break;
                case 4:
                    // 西北
                    moveClick(245, 230);
                    moveClick(377, 230);
                    break;
                case 6:
                    // 東南
                    moveClick(377, 230);
                    moveClick(245, 230);
                    break;
                case 7:
                    // 西
                    moveClick(278, 252);
                    moveClick(342, 210);
                    break;
                case 8:
                    // 西南
                    moveClick(310, 284);
                    moveClick(310, 185);
                    break;
                case 9:
                    // 南
                    moveClick(350, 260);
                    moveClick(285, 213);
                    break;
            }
            move(dirList);
        }

        private void moveClick(int x, int y)
        {
            dm.MoveToEx(x, y, 10, 10);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(400);
        }

        private void moveStop(object sender, EventArgs e)
        {
            if (th != null)
            {
                th.Abort();
            }
        }

        private void move2Run(object sender, EventArgs e)
        {
            int posE = Int32.Parse(tb_moveE.Text);
            int posS = Int32.Parse(tb_moveS.Text);
            int dir = cb_move10.SelectedIndex;
            
            th = new Thread(() =>
            {
                moveToPos(posE, posS);
                move2(dir, true);
            });
            th.Start();
        }

        private void move2(int dir, bool needClick) {
            string color = dm.GetColor(600, 10);
            if (color == "faf7ed" || color == "000000")
            {
                return;
            }

            int x1 = 342;
            int y1 = 210;
            int x2 = 270;
            int y2 = 252;

            switch (dir)
            {
                case 0:
                    x1 = 342;
                    y1 = 210;
                    x2 = 278;
                    y2 = 252;
                    break;
                case 1:
                    x1 = 278;
                    y1 = 252;
                    x2 = 342;
                    y2 = 210;
                    break;
                case 2:
                    x1 = 350;
                    y1 = 260;
                    x2 = 280;
                    y2 = 210;
                    break;
                case 3:
                    x1 = 280;
                    y1 = 210;
                    x2 = 350;
                    y2 = 260;
                    break;
                default:
                    break;
            }

            dm.MoveToEx(x1, y1, 10, 10);
            if (needClick) {
                dm.LeftDown();
            }
            Thread.Sleep(650);
            dm.MoveToEx(x2, y2, 10, 10);
            Thread.Sleep(550);
            if (needClick)
            {
                dm.LeftUp();
            }
            move2(dir, false);
        }


        ///////////
        /// 1A 藥劑
        ///////////
        private void medicine1aRun(object sender, EventArgs e)
        {
            MakeUIPos pos = getMakeUIPos();
            if (!pos.is_OK)
            {
                return;
            }

            th = new Thread(() =>
            {
                medicine1a(pos.btn_runX, pos.btn_runY, pos.item0X, pos.item0Y, 0);
            });
            th.Start();
        }

        private void medicine1a(int btn_runX, int btn_runY, int item0X, int item0Y, int index)
        {
            if (index >= 16)
            {
                AutoClosingMessageBox.Show("製作完畢", "CG", 1000);
                return;
            }

            // 材料A位置
            int itemAX = item0X + (index % 20) / 4 * 50;
            int itemAY = item0Y + index / 20 * 50;

            // 材料B位置
            int itemBX = item0X + 4 * 50;
            int itemBY = item0Y + 0 * 50;

            // 點材料A
            dm.MoveTo(itemAX, itemAY);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(200);

            // 點材料B
            dm.MoveTo(itemBX, itemBY);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(200);

            // 點製作
            dm.MoveTo(btn_runX, btn_runY);
            Thread.Sleep(100);
            dm.LeftClick();

            // 等15秒
            Thread.Sleep(16000);

            // 點重來
            dm.MoveTo(btn_runX, btn_runY);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(1000);

            // 下一輪
            medicine1a(btn_runX, btn_runY, item0X, item0Y, index + 1);
        }

        ///////////
        /// 鑑定
        ///////////
        public class IdentifyItem
        {
            public bool need = true;
            public bool complete = false;

            public int level = 0;
            public string text = "?";
        }

        string identifyItemsStr = "";
        string identifyResultStr = "";
        int identifyTotalLevel = 0;

        IdentifyItem[] identifyItems;

        private void identifyRun(object sender, EventArgs e)
        {
            speak("鑑定中，請稍後");

            dm.UseDict(0);

            // 建立item列表
            identifyItems = new IdentifyItem[20];
            for (int i = 0; i < 20; i++)
            {
                identifyItems[i] = new IdentifyItem();
            }

            // 打開鑑定技能
            identifyOpenSkill();

            // 取得所有需鑑定物並判斷等級
            th = new Thread(() =>
            {
                // 鑑定物資訊
                identifyGetInfo();
                // 開始鑑定
                identify();
            });
            th.Start();
        }

        private void identifyContinue(object sender, EventArgs e)
        {
            dm.UseDict(0);

            // 打開鑑定技能
            identifyOpenSkill();

            // 開始鑑定
            th = new Thread(() =>
            {
                identify();
            });
            th.Start();
        }

        private void identifyOpenSkill()
        {
            dm.MoveToEx(0, 0, 10, 5);

            // 關閉所有視窗
            closeAll();

            // 打開技能
            dm.KeyDownChar("CTRL");
            dm.KeyPressChar("E");
            Thread.Sleep(200);
            dm.KeyUpChar("CTRL");
            Thread.Sleep(300);
            dm.KeyPressChar("back");

            // 點鑑定
            for (int i = 0; i < 2; i++)
            {
                dm.MoveToEx(0, 0, 10, 5);
                Thread.Sleep(200);
                string skillPos = dm.FindPicE(0, 0, 640, 480, "assets/skillIdentify.bmp", "000000", 0.9, 0);
                string[] skillPosSplit = skillPos.Split('|');
                if (skillPosSplit[0] == "-1")
                {
                    dm.Play("assets/wav/鑑定找不到技能.wav");
                    return;
                }
                int skillX = Int32.Parse(skillPosSplit[1]) + 13;
                int skillY = Int32.Parse(skillPosSplit[2]) + 7;

                // 點鑑定
                dm.MoveToEx(skillX, skillY, 10, 5);
                Thread.Sleep(200);
                dm.LeftClick();
                Thread.Sleep(300);
            }

            // 視窗歸位
            dm.KeyDownChar("CTRL");
            dm.KeyPressChar("F12");
            Thread.Sleep(200);
            dm.KeyUpChar("CTRL");
            Thread.Sleep(200);
        }

        private void identifyGetInfo()
        {
            // 取得鑑定視窗位置
            string skillUIPos = dm.FindPicE(0, 0, 640, 480, "assets/skillUI.bmp", "000000", 0.9, 0);
            string[] skillUIPosSplit = skillUIPos.Split('|');
            if (skillUIPosSplit[0] == "-1")
            {
                dm.Play("assets/wav/鑑定找不到框.wav");
                return;
            }
            int skillUIX = Int32.Parse(skillUIPosSplit[1]) + 13;
            int skillUIY = Int32.Parse(skillUIPosSplit[2]) + 7;

            /*
            // 建立字庫用的
            Thread.Sleep(5000);
            int x1;
            int x2;
            int y1;
            int y2;
            string info;
            
            x1 = skillUIX - 68;
            x2 = skillUIX - 61;
            y1 = skillUIY + 38;
            y2 = skillUIY + 51;
            dm.Capture(x1, y1, x2, y2, "capture.bmp");
            info = dm.FetchWord(x1, y1, x2, y2, "000000", "8");
            dm.AddDict(2, info);

            dm.SaveDict(2, "8.txt");
            return;
            /**/

            for (int i = 0; i < 20; i++)
            {
                int x = skillUIX + 95 + (i % 5) * 50;
                int y = skillUIY + 35 + (i / 5) * 50;
                // 判斷有無東西
                dm.MoveToEx(0, 0, 10, 5);
                Thread.Sleep(100);
                // MessageBox.Show(dm.GetAveRGB(x - 10, y - 10, x + 10, y + 10));
                if (dm.GetAveRGB(x - 10, y - 10, x + 10, y + 10) == "fdf7e7")
                {
                    // 格子沒有東西
                    identifyItems[i].need = false;
                    identifyItems[i].complete = true;
                    identifyItems[i].text = "　";
                    continue;
                }
                // 點鑑定物
                dm.MoveToEx(x, y, 10, 5);
                Thread.Sleep(100);
                dm.LeftDoubleClick();
                Thread.Sleep(100);
                dm.MoveToEx(0, 0, 10, 5);
                Thread.Sleep(100);

                // 確認等級
                string mp = dm.Ocr(skillUIX - 70, skillUIY + 38, skillUIX - 53, skillUIY + 51, "000000", 1.0);
                if (mp == "")
                {
                    // 不需鑑定或無法鑑定
                    identifyItems[i].need = false;
                    identifyItems[i].complete = true;
                    identifyItems[i].text = "－";
                }
                else
                {
                    // identifyItems[i].level = Int32.Parse(mp) / 10;
                    identifyItems[i].level = Int32.Parse(mp);
                    if (identifyItems[i].level >= 10)
                    {
                        identifyItems[i].text = "Ｔ";
                    }
                    else
                    {
                        identifyItems[i].text = ((char)(identifyItems[i].level.ToString().ToCharArray()[0] + 65248)).ToString();
                    }
                }
            }

            // 顯示鑑定物等級及價格
            string strItems = "";
            string strResult = "";
            int totalLevel = 0;
            int totalNotNeed = 0;
            for (int i = 0; i < 20; i++)
            {
                strItems += identifyItems[i].text;
                if (i % 5 == 4)
                {
                    strItems += "\n";
                }

                totalLevel += identifyItems[i].level;
                if (identifyItems[i].text == "－")
                {
                    totalNotNeed++;
                }
            }
            identifyItemsStr = strItems;
            MethodInvoker mi = new MethodInvoker(this.identifyUpdateItems);
            this.BeginInvoke(mi, null);

            strResult += "總等：" + totalLevel.ToString();
            strResult += "\n不需：" + totalNotNeed.ToString();
            strResult += "\n總價：" + (totalLevel * 50).ToString();
            identifyResultStr = strResult;
            MethodInvoker mi2 = new MethodInvoker(this.identifyUpdateResult);
            this.BeginInvoke(mi2, null);

            identifyTotalLevel = totalLevel;
            MethodInvoker mi3 = new MethodInvoker(this.identifyUpdateTotalLevel);
            this.BeginInvoke(mi3, null);
        }

        private void identify()
        {
            string color = "";
            // 找到需鑑定的物品
            IdentifyItem item = null;
            int idx = 0;
            for (int i = 0; i < 20; i++)
            {
                if (!identifyItems[i].complete)
                {
                    item = identifyItems[i];
                    idx = i;
                    break;
                }
            }
            if (item == null)
            {
                dm.Play("assets/wav/鑑定完成了哦.wav");
                return;
            }

            // 取得鑑定視窗位置
            string skillUIPos = dm.FindPicE(0, 0, 640, 480, "assets/skillUI.bmp", "000000", 0.9, 0);
            string[] skillUIPosSplit = skillUIPos.Split('|');
            if (skillUIPosSplit[0] == "-1")
            {
                dm.Play("assets/wav/找不到鑑定框.wav");
                return;
            }
            int skillUIX = Int32.Parse(skillUIPosSplit[1]) + 13;
            int skillUIY = Int32.Parse(skillUIPosSplit[2]) + 7;

            // 點鑑定物
            int x = skillUIX + 95 + (idx % 5) * 50;
            int y = skillUIY + 35 + (idx / 5) * 50;
            dm.MoveToEx(x, y, 10, 5);
            Thread.Sleep(100);
            dm.LeftDoubleClick();
            Thread.Sleep(100);
            dm.MoveToEx(0, 0, 10, 5);
            Thread.Sleep(1000);

            color = dm.GetColor(skillUIX - 195, skillUIY + 200);
            // "333f75" 正常可執行
            // "f7f5ef" 不需鑑定
            // "242c52" 魔力不足
            if (color == "f7f5ef")
            {
                // 不需鑑定
                item.complete = true;
                identify();
                return;
            }
            else if (color == "242c52")
            {
                // 魔力不足
                dm.Play("assets/wav/鑑定魔力不足哦.wav");
                return;
            }
            else if (color != "333f75")
            {
                dm.Play("assets/wav/鑑定發生錯誤哦.wav");
                return;
            }

            // 點執行
            dm.MoveToEx(skillUIX - 195, skillUIY + 200, 10, 2);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(100);

            // 滑鼠移開
            dm.MoveToEx(skillUIX, skillUIY, 10, 2);
            Thread.Sleep(10000);

            // 檢查 鑑定重試的按鈕
            bool isFind = false;
            for (int i = 0; i < 30; i++)
            {
                if (dm.GetColor(skillUIX - 195, skillUIY + 200) == "333f75")
                {
                    isFind = true;
                    break;
                }
                Thread.Sleep(1000);
            }
            if (!isFind)
            {
                dm.Play("assets/wav/鑑定找不到蟲試的按鈕.wav");
                return;
            }

            // 點重試
            dm.MoveToEx(skillUIX - 195, skillUIY + 200, 10, 2);
            Thread.Sleep(100);
            dm.LeftClick();
            Thread.Sleep(500);

            // 下一個
            identify();
        }

        private void identifyUpdateItems()
        {
            lb_identifyItems.Text = identifyItemsStr;
        }

        private void identifyUpdateResult()
        {
            lb_identifyResult.Text = identifyResultStr;
        }

        private void identifyUpdateTotalLevel()
        {
            int total = identifyTotalLevel * 50;
            if (total > 500)
            {
                total = total / 100 * 100;
            }
            if (total > 1000)
            {
                total = total - total / 1000 * 100;
            }
            tb_identifyE.Text = "鑑定完成，共 " + identifyTotalLevel.ToString() + " 級，請點交易，收您 " + total.ToString();
        }

        // 鑑定歡迎訊息
        private void identifyWRun(object sender, EventArgs e)
        {
            speak(tb_identifyW.Text);
        }

        // 鑑定收錢訊息
        private void identifyERun(object sender, EventArgs e)
        {
            closeAll();
            speak(tb_identifyE.Text);
        }

        // 鑑定賣料理
        private void identifyTRun(object sender, EventArgs e)
        {
            int num1 = Int32.Parse(tb_identifyT1.Text);
            int num2 = Int32.Parse(tb_identifyT2.Text);
            int total = num1 * num2;

            speak("一組 " + num1.ToString() + "，共 " + num2.ToString() + " 組，收您 " + total.ToString());
        }

        ///////////
        /// 說話
        ///////////
        private void speakRun(object sender, EventArgs e)
        {
            speak(tb_speak.Text);
            tb_speak.Text = "";
        }

        ///////////
        /// 急救
        ///////////
        private void firstAidRun(object sender, EventArgs e)
        {
            int lv = Int32.Parse(tb_firstAid.Text);
            th = new Thread(() =>
            {
                firstAid(lv);
            });
            th.Start();
        }

        private void firstAid(int lv)
        {
            // 點急救
            dm.MoveToEx(0, 0, 10, 5);
            delay(200);
            string skillPos = dm.FindPicE(0, 0, 640, 480, "assets/skillFirstAid.bmp", "000000", 0.9, 1);
            string[] skillPosSplit = skillPos.Split('|');
            if (skillPosSplit[0] == "-1")
            {
                dm.Play("assets/wav/急救找不到技能.wav");
                return;
            }
            int skillX = Int32.Parse(skillPosSplit[1]) + 13;
            int skillY = Int32.Parse(skillPosSplit[2]) + 7;

            // 點急救
            dm.MoveToEx(skillX - 50, skillY + 35 + 15 * (lv - 1), 100, 5);
            delay(200);
            dm.LeftClick();
            delay(200);

            // 確認黑色
            bool isFind = false;
            for (int i = 0; i < 5; i++)
            {
                if (dm.GetColor(195, 160) == "000001")
                {
                    isFind = true;
                    break;
                }
                delay(1000);
            }
            if (!isFind)
            {
                dm.Play("assets/wav/急救找不到隊員窗.wav");
                return;
            }

            // 點隊員
            dm.MoveToEx(210, 200, 100, 10);
            delay(200);
            dm.LeftClick();
            delay(200);

            // 確認黑色
            isFind = false;
            for (int i = 0; i < 5; i++)
            {
                if (dm.GetColor(555, 125) == "000001")
                {
                    isFind = true;
                    break;
                }
                delay(1000);
            }
            if (!isFind)
            {
                dm.Play("assets/wav/急救找不到角色窗.wav");
                return;
            }

            // 點角色
            dm.MoveToEx(170, 177, 100, 10);
            delay(200);
            dm.LeftClick();
            delay(200);

            // 確認黑色
            isFind = false;
            for (int i = 0; i < 5; i++)
            {
                if (dm.GetColor(555, 125) != "000001")
                {
                    isFind = true;
                    break;
                }
                delay(1000);
            }
            if (!isFind)
            {
                dm.Play("assets/wav/急救角色窗沒關掉.wav");
                return;
            }

            // 重來
            firstAid(lv);
        }

    }
}