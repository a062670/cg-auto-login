﻿namespace CG_auto_login
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_bindWindow = new System.Windows.Forms.Button();
            this.lb_bindWindowStatus = new System.Windows.Forms.Label();
            this.panel_shoe1a = new System.Windows.Forms.Panel();
            this.btn_shoe1aRun = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel_capture = new System.Windows.Forms.Panel();
            this.btn_captureRun = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_captureY2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_captureX2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tb_captureY1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tb_captureX1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_linenRun = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_linen = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_identifyFishRun = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_identifyFish = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel_cooking2b = new System.Windows.Forms.Panel();
            this.btn_cooking2bRun = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_moveStop = new System.Windows.Forms.Button();
            this.cb_move8 = new System.Windows.Forms.CheckBox();
            this.btn_moveRun = new System.Windows.Forms.Button();
            this.cb_move9 = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cb_move7 = new System.Windows.Forms.CheckBox();
            this.cb_move6 = new System.Windows.Forms.CheckBox();
            this.cb_move1 = new System.Windows.Forms.CheckBox();
            this.cb_move4 = new System.Windows.Forms.CheckBox();
            this.cb_move3 = new System.Windows.Forms.CheckBox();
            this.cb_move2 = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.tb_moveS = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_moveE = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_medicine1aRun = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.panel_actions = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.tb_identifyT2 = new System.Windows.Forms.TextBox();
            this.btn_identifyTRun = new System.Windows.Forms.Button();
            this.tb_identifyT1 = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btn_identifyERun = new System.Windows.Forms.Button();
            this.tb_identifyE = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btn_identifyWRun = new System.Windows.Forms.Button();
            this.tb_identifyW = new System.Windows.Forms.TextBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lb_identifyResult = new System.Windows.Forms.Label();
            this.lb_identifyItems = new System.Windows.Forms.Label();
            this.btn_identifyContinue = new System.Windows.Forms.Button();
            this.btn_identifyRun = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_speakRun = new System.Windows.Forms.Button();
            this.tb_speak = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.cb_move10 = new System.Windows.Forms.ComboBox();
            this.btn_move2Run = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.btn_firstAidRun = new System.Windows.Forms.Button();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel_login = new System.Windows.Forms.Panel();
            this.btn_loginRun = new System.Windows.Forms.Button();
            this.cb_loginRow = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_loginCol = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.tb_firstAid = new System.Windows.Forms.TextBox();
            this.panel_shoe1a.SuspendLayout();
            this.panel_capture.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel_cooking2b.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel_actions.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel_login.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_bindWindow
            // 
            this.btn_bindWindow.Location = new System.Drawing.Point(12, 12);
            this.btn_bindWindow.Name = "btn_bindWindow";
            this.btn_bindWindow.Size = new System.Drawing.Size(70, 23);
            this.btn_bindWindow.TabIndex = 2;
            this.btn_bindWindow.Text = "綁定視窗";
            this.btn_bindWindow.UseVisualStyleBackColor = true;
            this.btn_bindWindow.Click += new System.EventHandler(this.bindWindow);
            // 
            // lb_bindWindowStatus
            // 
            this.lb_bindWindowStatus.AutoSize = true;
            this.lb_bindWindowStatus.Location = new System.Drawing.Point(88, 17);
            this.lb_bindWindowStatus.Name = "lb_bindWindowStatus";
            this.lb_bindWindowStatus.Size = new System.Drawing.Size(0, 12);
            this.lb_bindWindowStatus.TabIndex = 4;
            // 
            // panel_shoe1a
            // 
            this.panel_shoe1a.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel_shoe1a.Controls.Add(this.btn_shoe1aRun);
            this.panel_shoe1a.Controls.Add(this.label4);
            this.panel_shoe1a.Location = new System.Drawing.Point(5, 80);
            this.panel_shoe1a.Name = "panel_shoe1a";
            this.panel_shoe1a.Size = new System.Drawing.Size(147, 50);
            this.panel_shoe1a.TabIndex = 7;
            // 
            // btn_shoe1aRun
            // 
            this.btn_shoe1aRun.Location = new System.Drawing.Point(6, 19);
            this.btn_shoe1aRun.Name = "btn_shoe1aRun";
            this.btn_shoe1aRun.Size = new System.Drawing.Size(75, 23);
            this.btn_shoe1aRun.TabIndex = 1;
            this.btn_shoe1aRun.Text = "GO!";
            this.btn_shoe1aRun.UseVisualStyleBackColor = true;
            this.btn_shoe1aRun.Click += new System.EventHandler(this.shoe1aRun);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "1A鞋(18次)";
            // 
            // panel_capture
            // 
            this.panel_capture.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel_capture.Controls.Add(this.btn_captureRun);
            this.panel_capture.Controls.Add(this.label8);
            this.panel_capture.Controls.Add(this.tb_captureY2);
            this.panel_capture.Controls.Add(this.label9);
            this.panel_capture.Controls.Add(this.tb_captureX2);
            this.panel_capture.Controls.Add(this.label7);
            this.panel_capture.Controls.Add(this.tb_captureY1);
            this.panel_capture.Controls.Add(this.label6);
            this.panel_capture.Controls.Add(this.tb_captureX1);
            this.panel_capture.Controls.Add(this.label5);
            this.panel_capture.Location = new System.Drawing.Point(3, 4);
            this.panel_capture.Name = "panel_capture";
            this.panel_capture.Size = new System.Drawing.Size(168, 107);
            this.panel_capture.TabIndex = 8;
            // 
            // btn_captureRun
            // 
            this.btn_captureRun.Location = new System.Drawing.Point(6, 75);
            this.btn_captureRun.Name = "btn_captureRun";
            this.btn_captureRun.Size = new System.Drawing.Size(75, 23);
            this.btn_captureRun.TabIndex = 18;
            this.btn_captureRun.Text = "GO!";
            this.btn_captureRun.UseVisualStyleBackColor = true;
            this.btn_captureRun.Click += new System.EventHandler(this.captureRun);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(88, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 12);
            this.label8.TabIndex = 17;
            this.label8.Text = "y2";
            // 
            // tb_captureY2
            // 
            this.tb_captureY2.Location = new System.Drawing.Point(108, 47);
            this.tb_captureY2.Name = "tb_captureY2";
            this.tb_captureY2.Size = new System.Drawing.Size(50, 22);
            this.tb_captureY2.TabIndex = 16;
            this.tb_captureY2.Text = "480";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 12);
            this.label9.TabIndex = 15;
            this.label9.Text = "x2";
            // 
            // tb_captureX2
            // 
            this.tb_captureX2.Location = new System.Drawing.Point(27, 47);
            this.tb_captureX2.Name = "tb_captureX2";
            this.tb_captureX2.Size = new System.Drawing.Size(50, 22);
            this.tb_captureX2.TabIndex = 14;
            this.tb_captureX2.Text = "640";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(88, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "y1";
            // 
            // tb_captureY1
            // 
            this.tb_captureY1.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_captureY1.Location = new System.Drawing.Point(108, 19);
            this.tb_captureY1.Name = "tb_captureY1";
            this.tb_captureY1.Size = new System.Drawing.Size(50, 22);
            this.tb_captureY1.TabIndex = 12;
            this.tb_captureY1.Text = "0";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "x1";
            // 
            // tb_captureX1
            // 
            this.tb_captureX1.Location = new System.Drawing.Point(27, 19);
            this.tb_captureX1.Name = "tb_captureX1";
            this.tb_captureX1.Size = new System.Drawing.Size(50, 22);
            this.tb_captureX1.TabIndex = 10;
            this.tb_captureX1.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "截圖(開發用)";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Controls.Add(this.btn_linenRun);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.tb_linen);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(186, 50);
            this.panel1.TabIndex = 19;
            // 
            // btn_linenRun
            // 
            this.btn_linenRun.Location = new System.Drawing.Point(88, 19);
            this.btn_linenRun.Name = "btn_linenRun";
            this.btn_linenRun.Size = new System.Drawing.Size(75, 23);
            this.btn_linenRun.TabIndex = 14;
            this.btn_linenRun.Text = "GO!";
            this.btn_linenRun.UseVisualStyleBackColor = true;
            this.btn_linenRun.Click += new System.EventHandler(this.linenRun);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(62, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 13;
            this.label11.Text = "次";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 12);
            this.label10.TabIndex = 12;
            this.label10.Text = "買麻布";
            // 
            // tb_linen
            // 
            this.tb_linen.Location = new System.Drawing.Point(6, 19);
            this.tb_linen.Name = "tb_linen";
            this.tb_linen.Size = new System.Drawing.Size(50, 22);
            this.tb_linen.TabIndex = 11;
            this.tb_linen.Text = "20";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel2.Controls.Add(this.btn_identifyFishRun);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.tb_identifyFish);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Location = new System.Drawing.Point(6, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(147, 49);
            this.panel2.TabIndex = 9;
            // 
            // btn_identifyFishRun
            // 
            this.btn_identifyFishRun.Location = new System.Drawing.Point(75, 19);
            this.btn_identifyFishRun.Name = "btn_identifyFishRun";
            this.btn_identifyFishRun.Size = new System.Drawing.Size(50, 23);
            this.btn_identifyFishRun.TabIndex = 17;
            this.btn_identifyFishRun.Text = "GO!";
            this.btn_identifyFishRun.UseVisualStyleBackColor = true;
            this.btn_identifyFishRun.Click += new System.EventHandler(this.identifyFishRun);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(52, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 16;
            this.label13.Text = "次";
            // 
            // tb_identifyFish
            // 
            this.tb_identifyFish.Location = new System.Drawing.Point(7, 19);
            this.tb_identifyFish.Name = "tb_identifyFish";
            this.tb_identifyFish.Size = new System.Drawing.Size(40, 22);
            this.tb_identifyFish.TabIndex = 15;
            this.tb_identifyFish.Text = "20";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 4);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 12);
            this.label12.TabIndex = 0;
            this.label12.Text = "魚店鑑定";
            // 
            // panel_cooking2b
            // 
            this.panel_cooking2b.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel_cooking2b.Controls.Add(this.btn_cooking2bRun);
            this.panel_cooking2b.Controls.Add(this.label14);
            this.panel_cooking2b.Location = new System.Drawing.Point(5, 136);
            this.panel_cooking2b.Name = "panel_cooking2b";
            this.panel_cooking2b.Size = new System.Drawing.Size(158, 50);
            this.panel_cooking2b.TabIndex = 20;
            // 
            // btn_cooking2bRun
            // 
            this.btn_cooking2bRun.Location = new System.Drawing.Point(6, 19);
            this.btn_cooking2bRun.Name = "btn_cooking2bRun";
            this.btn_cooking2bRun.Size = new System.Drawing.Size(75, 23);
            this.btn_cooking2bRun.TabIndex = 1;
            this.btn_cooking2bRun.Text = "GO!";
            this.btn_cooking2bRun.UseVisualStyleBackColor = true;
            this.btn_cooking2bRun.Click += new System.EventHandler(this.cooking2bRun);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(4, 4);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 12);
            this.label14.TabIndex = 0;
            this.label14.Text = "法國麵包(10次)";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel3.Controls.Add(this.btn_moveStop);
            this.panel3.Controls.Add(this.cb_move8);
            this.panel3.Controls.Add(this.btn_moveRun);
            this.panel3.Controls.Add(this.cb_move9);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.cb_move7);
            this.panel3.Controls.Add(this.cb_move6);
            this.panel3.Controls.Add(this.cb_move1);
            this.panel3.Controls.Add(this.cb_move4);
            this.panel3.Controls.Add(this.cb_move3);
            this.panel3.Controls.Add(this.cb_move2);
            this.panel3.Location = new System.Drawing.Point(3, 48);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(163, 84);
            this.panel3.TabIndex = 21;
            // 
            // btn_moveStop
            // 
            this.btn_moveStop.Location = new System.Drawing.Point(72, 52);
            this.btn_moveStop.Name = "btn_moveStop";
            this.btn_moveStop.Size = new System.Drawing.Size(75, 23);
            this.btn_moveStop.TabIndex = 30;
            this.btn_moveStop.Text = "STOP";
            this.btn_moveStop.UseVisualStyleBackColor = true;
            this.btn_moveStop.Click += new System.EventHandler(this.moveStop);
            // 
            // cb_move8
            // 
            this.cb_move8.AutoSize = true;
            this.cb_move8.Checked = true;
            this.cb_move8.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_move8.Location = new System.Drawing.Point(28, 61);
            this.cb_move8.Name = "cb_move8";
            this.cb_move8.Size = new System.Drawing.Size(15, 14);
            this.cb_move8.TabIndex = 28;
            this.cb_move8.UseVisualStyleBackColor = true;
            // 
            // btn_moveRun
            // 
            this.btn_moveRun.Location = new System.Drawing.Point(72, 21);
            this.btn_moveRun.Name = "btn_moveRun";
            this.btn_moveRun.Size = new System.Drawing.Size(75, 23);
            this.btn_moveRun.TabIndex = 1;
            this.btn_moveRun.Text = "GO!";
            this.btn_moveRun.UseVisualStyleBackColor = true;
            this.btn_moveRun.Click += new System.EventHandler(this.moveRun);
            // 
            // cb_move9
            // 
            this.cb_move9.AutoSize = true;
            this.cb_move9.Checked = true;
            this.cb_move9.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_move9.Location = new System.Drawing.Point(49, 61);
            this.cb_move9.Name = "cb_move9";
            this.cb_move9.Size = new System.Drawing.Size(15, 14);
            this.cb_move9.TabIndex = 29;
            this.cb_move9.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(4, 4);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 12);
            this.label15.TabIndex = 0;
            this.label15.Text = "移動1";
            // 
            // cb_move7
            // 
            this.cb_move7.AutoSize = true;
            this.cb_move7.Checked = true;
            this.cb_move7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_move7.Location = new System.Drawing.Point(7, 61);
            this.cb_move7.Name = "cb_move7";
            this.cb_move7.Size = new System.Drawing.Size(15, 14);
            this.cb_move7.TabIndex = 27;
            this.cb_move7.UseVisualStyleBackColor = true;
            // 
            // cb_move6
            // 
            this.cb_move6.AutoSize = true;
            this.cb_move6.Checked = true;
            this.cb_move6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_move6.Location = new System.Drawing.Point(49, 41);
            this.cb_move6.Name = "cb_move6";
            this.cb_move6.Size = new System.Drawing.Size(15, 14);
            this.cb_move6.TabIndex = 26;
            this.cb_move6.UseVisualStyleBackColor = true;
            // 
            // cb_move1
            // 
            this.cb_move1.AutoSize = true;
            this.cb_move1.Checked = true;
            this.cb_move1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_move1.Location = new System.Drawing.Point(7, 21);
            this.cb_move1.Name = "cb_move1";
            this.cb_move1.Size = new System.Drawing.Size(15, 14);
            this.cb_move1.TabIndex = 2;
            this.cb_move1.UseVisualStyleBackColor = true;
            // 
            // cb_move4
            // 
            this.cb_move4.AutoSize = true;
            this.cb_move4.Checked = true;
            this.cb_move4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_move4.Location = new System.Drawing.Point(7, 41);
            this.cb_move4.Name = "cb_move4";
            this.cb_move4.Size = new System.Drawing.Size(15, 14);
            this.cb_move4.TabIndex = 24;
            this.cb_move4.UseVisualStyleBackColor = true;
            // 
            // cb_move3
            // 
            this.cb_move3.AutoSize = true;
            this.cb_move3.Checked = true;
            this.cb_move3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_move3.Location = new System.Drawing.Point(49, 21);
            this.cb_move3.Name = "cb_move3";
            this.cb_move3.Size = new System.Drawing.Size(15, 14);
            this.cb_move3.TabIndex = 4;
            this.cb_move3.UseVisualStyleBackColor = true;
            // 
            // cb_move2
            // 
            this.cb_move2.AutoSize = true;
            this.cb_move2.Checked = true;
            this.cb_move2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_move2.Location = new System.Drawing.Point(28, 21);
            this.cb_move2.Name = "cb_move2";
            this.cb_move2.Size = new System.Drawing.Size(15, 14);
            this.cb_move2.TabIndex = 3;
            this.cb_move2.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(63, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 34;
            this.label18.Text = "南";
            // 
            // tb_moveS
            // 
            this.tb_moveS.Location = new System.Drawing.Point(84, 13);
            this.tb_moveS.Name = "tb_moveS";
            this.tb_moveS.Size = new System.Drawing.Size(36, 22);
            this.tb_moveS.TabIndex = 33;
            this.tb_moveS.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 12);
            this.label1.TabIndex = 32;
            this.label1.Text = "東";
            // 
            // tb_moveE
            // 
            this.tb_moveE.Location = new System.Drawing.Point(25, 13);
            this.tb_moveE.Name = "tb_moveE";
            this.tb_moveE.Size = new System.Drawing.Size(36, 22);
            this.tb_moveE.TabIndex = 31;
            this.tb_moveE.Text = "0";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel4.Controls.Add(this.btn_medicine1aRun);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Location = new System.Drawing.Point(5, 192);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(147, 50);
            this.panel4.TabIndex = 22;
            // 
            // btn_medicine1aRun
            // 
            this.btn_medicine1aRun.Location = new System.Drawing.Point(6, 19);
            this.btn_medicine1aRun.Name = "btn_medicine1aRun";
            this.btn_medicine1aRun.Size = new System.Drawing.Size(75, 23);
            this.btn_medicine1aRun.TabIndex = 1;
            this.btn_medicine1aRun.Text = "GO!";
            this.btn_medicine1aRun.UseVisualStyleBackColor = true;
            this.btn_medicine1aRun.Click += new System.EventHandler(this.medicine1aRun);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(4, 4);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 12);
            this.label16.TabIndex = 0;
            this.label16.Text = "1A藥劑(16次)";
            // 
            // panel_actions
            // 
            this.panel_actions.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel_actions.Controls.Add(this.tabControl1);
            this.panel_actions.Enabled = false;
            this.panel_actions.Location = new System.Drawing.Point(14, 41);
            this.panel_actions.Name = "panel_actions";
            this.panel_actions.Size = new System.Drawing.Size(758, 167);
            this.panel_actions.TabIndex = 23;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(752, 161);
            this.tabControl1.TabIndex = 25;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel9);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Controls.Add(this.panel7);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(744, 135);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "鑑定";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel9.Controls.Add(this.tb_identifyT2);
            this.panel9.Controls.Add(this.btn_identifyTRun);
            this.panel9.Controls.Add(this.tb_identifyT1);
            this.panel9.Location = new System.Drawing.Point(514, 6);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(65, 123);
            this.panel9.TabIndex = 27;
            // 
            // tb_identifyT2
            // 
            this.tb_identifyT2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_identifyT2.Location = new System.Drawing.Point(7, 38);
            this.tb_identifyT2.Multiline = true;
            this.tb_identifyT2.Name = "tb_identifyT2";
            this.tb_identifyT2.Size = new System.Drawing.Size(50, 23);
            this.tb_identifyT2.TabIndex = 18;
            this.tb_identifyT2.Text = "10";
            // 
            // btn_identifyTRun
            // 
            this.btn_identifyTRun.Location = new System.Drawing.Point(7, 97);
            this.btn_identifyTRun.Name = "btn_identifyTRun";
            this.btn_identifyTRun.Size = new System.Drawing.Size(50, 23);
            this.btn_identifyTRun.TabIndex = 17;
            this.btn_identifyTRun.Text = "GO!";
            this.btn_identifyTRun.UseVisualStyleBackColor = true;
            this.btn_identifyTRun.Click += new System.EventHandler(this.identifyTRun);
            // 
            // tb_identifyT1
            // 
            this.tb_identifyT1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_identifyT1.Location = new System.Drawing.Point(7, 8);
            this.tb_identifyT1.Multiline = true;
            this.tb_identifyT1.Name = "tb_identifyT1";
            this.tb_identifyT1.Size = new System.Drawing.Size(50, 24);
            this.tb_identifyT1.TabIndex = 15;
            this.tb_identifyT1.Text = "1350";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel8.Controls.Add(this.btn_identifyERun);
            this.panel8.Controls.Add(this.tb_identifyE);
            this.panel8.Location = new System.Drawing.Point(373, 6);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(134, 123);
            this.panel8.TabIndex = 26;
            // 
            // btn_identifyERun
            // 
            this.btn_identifyERun.Location = new System.Drawing.Point(7, 97);
            this.btn_identifyERun.Name = "btn_identifyERun";
            this.btn_identifyERun.Size = new System.Drawing.Size(50, 23);
            this.btn_identifyERun.TabIndex = 17;
            this.btn_identifyERun.Text = "GO!";
            this.btn_identifyERun.UseVisualStyleBackColor = true;
            this.btn_identifyERun.Click += new System.EventHandler(this.identifyERun);
            // 
            // tb_identifyE
            // 
            this.tb_identifyE.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_identifyE.Location = new System.Drawing.Point(7, 8);
            this.tb_identifyE.Multiline = true;
            this.tb_identifyE.Name = "tb_identifyE";
            this.tb_identifyE.Size = new System.Drawing.Size(119, 88);
            this.tb_identifyE.TabIndex = 15;
            this.tb_identifyE.Text = "鑑定完成，請點交易，收您 1000 ";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel7.Controls.Add(this.btn_identifyWRun);
            this.panel7.Controls.Add(this.tb_identifyW);
            this.panel7.Location = new System.Drawing.Point(169, 6);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(196, 123);
            this.panel7.TabIndex = 25;
            // 
            // btn_identifyWRun
            // 
            this.btn_identifyWRun.Location = new System.Drawing.Point(7, 97);
            this.btn_identifyWRun.Name = "btn_identifyWRun";
            this.btn_identifyWRun.Size = new System.Drawing.Size(50, 23);
            this.btn_identifyWRun.TabIndex = 17;
            this.btn_identifyWRun.Text = "GO!";
            this.btn_identifyWRun.UseVisualStyleBackColor = true;
            this.btn_identifyWRun.Click += new System.EventHandler(this.identifyWRun);
            // 
            // tb_identifyW
            // 
            this.tb_identifyW.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_identifyW.Location = new System.Drawing.Point(7, 8);
            this.tb_identifyW.Multiline = true;
            this.tb_identifyW.Name = "tb_identifyW";
            this.tb_identifyW.Size = new System.Drawing.Size(180, 88);
            this.tb_identifyW.TabIndex = 15;
            this.tb_identifyW.Text = "安安，鑑定請點交易，最多 19 格";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel5.Controls.Add(this.lb_identifyResult);
            this.panel5.Controls.Add(this.lb_identifyItems);
            this.panel5.Controls.Add(this.btn_identifyContinue);
            this.panel5.Controls.Add(this.btn_identifyRun);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Location = new System.Drawing.Point(6, 6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(157, 105);
            this.panel5.TabIndex = 23;
            // 
            // lb_identifyResult
            // 
            this.lb_identifyResult.AutoSize = true;
            this.lb_identifyResult.Location = new System.Drawing.Point(79, 48);
            this.lb_identifyResult.Name = "lb_identifyResult";
            this.lb_identifyResult.Size = new System.Drawing.Size(0, 12);
            this.lb_identifyResult.TabIndex = 4;
            // 
            // lb_identifyItems
            // 
            this.lb_identifyItems.AutoSize = true;
            this.lb_identifyItems.Location = new System.Drawing.Point(6, 49);
            this.lb_identifyItems.Name = "lb_identifyItems";
            this.lb_identifyItems.Size = new System.Drawing.Size(0, 12);
            this.lb_identifyItems.TabIndex = 3;
            // 
            // btn_identifyContinue
            // 
            this.btn_identifyContinue.Location = new System.Drawing.Point(70, 18);
            this.btn_identifyContinue.Name = "btn_identifyContinue";
            this.btn_identifyContinue.Size = new System.Drawing.Size(60, 23);
            this.btn_identifyContinue.TabIndex = 2;
            this.btn_identifyContinue.Text = "Continue";
            this.btn_identifyContinue.UseVisualStyleBackColor = true;
            this.btn_identifyContinue.Click += new System.EventHandler(this.identifyContinue);
            // 
            // btn_identifyRun
            // 
            this.btn_identifyRun.Location = new System.Drawing.Point(5, 18);
            this.btn_identifyRun.Name = "btn_identifyRun";
            this.btn_identifyRun.Size = new System.Drawing.Size(60, 23);
            this.btn_identifyRun.TabIndex = 1;
            this.btn_identifyRun.Text = "GO!";
            this.btn_identifyRun.UseVisualStyleBackColor = true;
            this.btn_identifyRun.Click += new System.EventHandler(this.identifyRun);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "鑑定";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel6.Controls.Add(this.btn_speakRun);
            this.panel6.Controls.Add(this.tb_speak);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Location = new System.Drawing.Point(585, 6);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(156, 123);
            this.panel6.TabIndex = 24;
            // 
            // btn_speakRun
            // 
            this.btn_speakRun.Location = new System.Drawing.Point(8, 98);
            this.btn_speakRun.Name = "btn_speakRun";
            this.btn_speakRun.Size = new System.Drawing.Size(50, 23);
            this.btn_speakRun.TabIndex = 17;
            this.btn_speakRun.Text = "GO!";
            this.btn_speakRun.UseVisualStyleBackColor = true;
            this.btn_speakRun.Click += new System.EventHandler(this.speakRun);
            // 
            // tb_speak
            // 
            this.tb_speak.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tb_speak.Location = new System.Drawing.Point(7, 19);
            this.tb_speak.Multiline = true;
            this.tb_speak.Name = "tb_speak";
            this.tb_speak.Size = new System.Drawing.Size(146, 77);
            this.tb_speak.TabIndex = 15;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 4);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 12);
            this.label19.TabIndex = 0;
            this.label19.Text = "說話";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(744, 135);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "魚店";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(744, 135);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "買麻布";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel10);
            this.tabPage4.Controls.Add(this.label18);
            this.tabPage4.Controls.Add(this.panel3);
            this.tabPage4.Controls.Add(this.tb_moveS);
            this.tabPage4.Controls.Add(this.tb_moveE);
            this.tabPage4.Controls.Add(this.label1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(744, 135);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "封印遇怪";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel10.Controls.Add(this.cb_move10);
            this.panel10.Controls.Add(this.btn_move2Run);
            this.panel10.Controls.Add(this.label20);
            this.panel10.Location = new System.Drawing.Point(172, 48);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(163, 84);
            this.panel10.TabIndex = 31;
            // 
            // cb_move10
            // 
            this.cb_move10.FormattingEnabled = true;
            this.cb_move10.Items.AddRange(new object[] {
            "東西",
            "西東",
            "南北",
            "北南"});
            this.cb_move10.Location = new System.Drawing.Point(6, 20);
            this.cb_move10.Name = "cb_move10";
            this.cb_move10.Size = new System.Drawing.Size(60, 20);
            this.cb_move10.TabIndex = 31;
            // 
            // btn_move2Run
            // 
            this.btn_move2Run.Location = new System.Drawing.Point(72, 21);
            this.btn_move2Run.Name = "btn_move2Run";
            this.btn_move2Run.Size = new System.Drawing.Size(75, 23);
            this.btn_move2Run.TabIndex = 1;
            this.btn_move2Run.Text = "GO!";
            this.btn_move2Run.UseVisualStyleBackColor = true;
            this.btn_move2Run.Click += new System.EventHandler(this.move2Run);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(4, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(35, 12);
            this.label20.TabIndex = 0;
            this.label20.Text = "移動2";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.panel_capture);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(744, 135);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "截圖";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.label21);
            this.tabPage7.Controls.Add(this.tb_firstAid);
            this.tabPage7.Controls.Add(this.btn_firstAidRun);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(744, 135);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "急救";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // btn_firstAidRun
            // 
            this.btn_firstAidRun.Location = new System.Drawing.Point(125, 6);
            this.btn_firstAidRun.Name = "btn_firstAidRun";
            this.btn_firstAidRun.Size = new System.Drawing.Size(75, 23);
            this.btn_firstAidRun.TabIndex = 0;
            this.btn_firstAidRun.Text = "GO!";
            this.btn_firstAidRun.UseVisualStyleBackColor = true;
            this.btn_firstAidRun.Click += new System.EventHandler(this.firstAidRun);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.panel_login);
            this.tabPage6.Controls.Add(this.panel_shoe1a);
            this.tabPage6.Controls.Add(this.panel_cooking2b);
            this.tabPage6.Controls.Add(this.panel4);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(744, 135);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "用不到";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panel_login
            // 
            this.panel_login.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel_login.Controls.Add(this.btn_loginRun);
            this.panel_login.Controls.Add(this.cb_loginRow);
            this.panel_login.Controls.Add(this.label3);
            this.panel_login.Controls.Add(this.label2);
            this.panel_login.Controls.Add(this.cb_loginCol);
            this.panel_login.Location = new System.Drawing.Point(4, 8);
            this.panel_login.Name = "panel_login";
            this.panel_login.Size = new System.Drawing.Size(173, 66);
            this.panel_login.TabIndex = 6;
            // 
            // btn_loginRun
            // 
            this.btn_loginRun.Location = new System.Drawing.Point(7, 43);
            this.btn_loginRun.Name = "btn_loginRun";
            this.btn_loginRun.Size = new System.Drawing.Size(75, 23);
            this.btn_loginRun.TabIndex = 7;
            this.btn_loginRun.Text = "GO!";
            this.btn_loginRun.UseVisualStyleBackColor = true;
            this.btn_loginRun.Click += new System.EventHandler(this.loginRun);
            // 
            // cb_loginRow
            // 
            this.cb_loginRow.FormattingEnabled = true;
            this.cb_loginRow.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.cb_loginRow.Location = new System.Drawing.Point(97, 19);
            this.cb_loginRow.Name = "cb_loginRow";
            this.cb_loginRow.Size = new System.Drawing.Size(50, 20);
            this.cb_loginRow.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "排之";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "登登樂";
            // 
            // cb_loginCol
            // 
            this.cb_loginCol.FormattingEnabled = true;
            this.cb_loginCol.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cb_loginCol.Location = new System.Drawing.Point(6, 19);
            this.cb_loginCol.Name = "cb_loginCol";
            this.cb_loginCol.Size = new System.Drawing.Size(50, 20);
            this.cb_loginCol.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(697, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 31;
            this.button1.Text = "STOP";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.stopTh);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(10, 10);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 12);
            this.label21.TabIndex = 15;
            this.label21.Text = "急救等級";
            // 
            // tb_firstAid
            // 
            this.tb_firstAid.Location = new System.Drawing.Point(69, 6);
            this.tb_firstAid.Name = "tb_firstAid";
            this.tb_firstAid.Size = new System.Drawing.Size(50, 22);
            this.tb_firstAid.TabIndex = 14;
            this.tb_firstAid.Text = "8";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 211);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel_actions);
            this.Controls.Add(this.lb_bindWindowStatus);
            this.Controls.Add(this.btn_bindWindow);
            this.Name = "Form1";
            this.Text = "Auto login";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.initForm);
            this.panel_shoe1a.ResumeLayout(false);
            this.panel_shoe1a.PerformLayout();
            this.panel_capture.ResumeLayout(false);
            this.panel_capture.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel_cooking2b.ResumeLayout(false);
            this.panel_cooking2b.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel_actions.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.panel_login.ResumeLayout(false);
            this.panel_login.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_bindWindow;
        private System.Windows.Forms.Label lb_bindWindowStatus;
        private System.Windows.Forms.Panel panel_shoe1a;
        private System.Windows.Forms.Button btn_shoe1aRun;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel_capture;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_captureY2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_captureX2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tb_captureY1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tb_captureX1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_captureRun;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_linenRun;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_linen;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_identifyFishRun;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_identifyFish;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel_cooking2b;
        private System.Windows.Forms.Button btn_cooking2bRun;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_moveRun;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_medicine1aRun;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel_actions;
        private System.Windows.Forms.Button btn_moveStop;
        private System.Windows.Forms.CheckBox cb_move8;
        private System.Windows.Forms.CheckBox cb_move9;
        private System.Windows.Forms.CheckBox cb_move7;
        private System.Windows.Forms.CheckBox cb_move6;
        private System.Windows.Forms.CheckBox cb_move1;
        private System.Windows.Forms.CheckBox cb_move4;
        private System.Windows.Forms.CheckBox cb_move3;
        private System.Windows.Forms.CheckBox cb_move2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lb_identifyItems;
        private System.Windows.Forms.Button btn_identifyContinue;
        private System.Windows.Forms.Button btn_identifyRun;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lb_identifyResult;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btn_speakRun;
        private System.Windows.Forms.TextBox tb_speak;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Panel panel_login;
        private System.Windows.Forms.Button btn_loginRun;
        private System.Windows.Forms.ComboBox cb_loginRow;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_loginCol;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button btn_firstAidRun;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btn_identifyWRun;
        private System.Windows.Forms.TextBox tb_identifyW;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox tb_identifyT2;
        private System.Windows.Forms.Button btn_identifyTRun;
        private System.Windows.Forms.TextBox tb_identifyT1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button btn_identifyERun;
        private System.Windows.Forms.TextBox tb_identifyE;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tb_moveS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_moveE;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.ComboBox cb_move10;
        private System.Windows.Forms.Button btn_move2Run;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox tb_firstAid;
    }
}

